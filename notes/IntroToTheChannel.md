# Channel Introduction

### Date released

Short video going over what this channel is for and why I created it.

[link to rumble](https://rumble.com/vphj6u-introduction-to-the-channel.html)

## High level

-   There is a ton of information about data science and deep learning in python out there.
-   It is not always in the best location and can be difficult to find or know where to get started.
-   The idea here is to make some basic tutorials to get people started from the ground up.
	-   Basically a walk through so hopefully someone can learn from my mistakes/battle.

## Sources

* The [gitlab location](https://gitlab.com/a6093/tutorialnotes) where notes and code will live.