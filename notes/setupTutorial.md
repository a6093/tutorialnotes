# Setting up the environment

### Date released

In this episode we will go through the process of setting up a virtual environment for deep learning in python.  There are many ways to do this and none of them are perfect.

Anaconda is great for beginners as it take care of package dependencies and has a simple to use interface for updating.  Directly installing python and using `pip` has more flexibility but is easy to break things if you miss install something.  If you choose anaconda **Do not use `pip`.  It will break things badly!**

[Linux Version](https://rumble.com/vq69c4-linux-environment-setup.html)

[Windows Version](NEED)

## High level

-   Operating system…
    -   Windows is easier to setup but limits certain things.  (This is becoming less of an issue as support for python increases)
    -   You can do anything with Linux.  It is easy to get setup and easier to get it setup badly so it breaks.
    -   MacOS I don’t have experience with but from what I know it is similar in setup to Linux but more stable.

-   Anaconda vs direct install.
    -   Anaconda is easier to use.  Direct install has more flexibility.
    -   If you are a student, solo practitioner, or researcher you can use anaconda for free still.
    -   Corporations need to get a subscription license.

-   Install procedure for both.
-   The virtual environment.
-   Library installation.

## Commands used

### Anaconda

To open up the anaconda prompt search in programs (start menu for windows, show applications for ubuntu) and run it.

Updating all installed packages in anaconda:

```
conda update --all
```

To install packages in conda:

```
conda install matplotlib numpy pandas tensorflow imageio
```



### Direct installed

To install packages with pip:  (_Note if you are running on linux you will likely need to run python3 not python_)

```
python -m pip install matplotlib numpy pandas trensorflow imageio
```

`pip` does not allow you to update all the package at once.  Instead you have to run `python -m pip list --outdated` to find out which package are out of date.  Then you update each package individually.  **Be aware though `pip` does not check other packages** you can update a package that will break other packages.  After you update a package you should run `pip check` which will check for any unmet or broken dependencies.  If any come up you should fix them by installing the version required.

## Dependencies

-   [python](https://www.python.org/downloads/) or [anaconda python](https://www.anaconda.com/products/individual) downloaded.
    

## Virtual environment

We want to use a virtual environment because it allows us to isolate the python packages we are using from other installs of python.  This insures the system level install is kept separate from the version we are developing with.  This is VERY important on Linux based systems.  If you mess too much with the system install you may need to reinstall the system to fix the issues.

If you use anaconda virtual environments are all handled by anaconda.  If you use a direct install you will need to manually do this.  The process is not difficult it just adds an extra setup to the install process.

## Sources

* [Python website for direct install](https://www.python.org/downloads/)
* [Anaconda website](https://www.anaconda.com/products/individual)
  * [Anaconda pricing for commercial](https://www.anaconda.com/pricing)
* [Virtualenv vs venv](https://www.infoworld.com/article/3239675/virtualenv-and-venv-python-virtual-environments-explained.html)
* [virtualenv pypa page](https://virtualenv.pypa.io/en/latest/)