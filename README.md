# Tutorial Notes

This repo contains both the notes and source code for: https://rumble.com/c/c-1205933

## Roadmap
As I get content on rumble (tutorials for python) this repo will contain notes, sources, and source code for anyone who wishes to use it.

### How can you help

Have an idea for a new episode?  I’d love to hear it!  [Open an issue](https://gitlab.com/a6093/tutorialnotes/-/issues/new) and let me know about it.

Find something wrong with the code?  Does it break on your system?  That is no good.  [Open an issue](https://gitlab.com/a6093/tutorialnotes/-/issues/new) and let’s see if we can figure it out.

## License
For open source projects, say how it is licensed.

## Project status
This is still in the early stages of setting up on rumble and getting ready to start launching tutorial and how to videos related to python and data science.

